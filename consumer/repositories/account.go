package repositories

import "gorm.io/gorm"

type BankAccount struct {
	ID            string
	AccountHolder string
	AccountType   int
	Balance       float64
}

type AccountRepository interface {
	Save(bankAccount BankAccount) error
	Delete(id string) error
	FindAll() (bankAccount []BankAccount, err error)
	FindByID(id string) (bankAccount BankAccount, err error)
}

type accountRepository struct {
	db *gorm.DB
}

func NewAccountRepository(db *gorm.DB) AccountRepository {
	db.Table("banks").AutoMigrate(&BankAccount{})
	return accountRepository{db}

}

func (a accountRepository) Save(bankAccount BankAccount) error {
	return a.db.Table("banks").Save(bankAccount).Error
}

func (a accountRepository) Delete(id string) error {
	return a.db.Table("banks").Where("id=?", id).Delete(&BankAccount{}).Error
}

func (a accountRepository) FindAll() (bankAccount []BankAccount, err error) {
	err = a.db.Table("banks").Find(&bankAccount).Error
	return bankAccount, err
}

func (a accountRepository) FindByID(id string) (bankAccount BankAccount, err error) {
	err = a.db.Table("banks").Where("id=?", id).First(&bankAccount).Error
	return bankAccount, err
}
