package services

import "github.com/Shopify/sarama"

type consumeHandler struct {
	eventHandler EventHandler
}

func NewConsumerHandler(eventHandler EventHandler) sarama.ConsumerGroupHandler {
	return consumeHandler{eventHandler}
}

func (c consumeHandler) Setup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (c consumeHandler) Cleanup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (c consumeHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		c.eventHandler.Handle(msg.Topic, msg.Value)
		session.MarkMessage(msg, "")
	}
	return nil
}
